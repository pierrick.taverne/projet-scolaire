# -*- coding: utf-8 -*-
#TAVERNE Pierrick

#Import de bibliothèque
from tkinter import *
from jeu_de_la_vie import *
import time

#Constantes
DELAI=1000
btn_MORT='BLACK'
btn_VIVANT='WHITE'
btn_width='3'
btn_height='2'


#Classe du jeu
class VueJeuDeLaVie:

	#Constructeur
	def __init__(self,jeu):

		#Attributs
		self.jeu=jeu
		self.fen=Tk()
		self.fen.title("Jeu de la vie")
		self.nb_lig=jeu.nb_lig
		self.nb_col=jeu.nb_col
		self.grille=Frame(self.fen)
		self.entryGrille=Frame(self.fen)
		self.alea=Frame(self.fen)
		self.cmd=Frame(self.fen)
		self.debut=True
		self.boucle=True

		#Création des boutons de la grille
		self.btns=[]
		for i in range(self.nb_lig):
			uneLigne=[]
			for j in range(self.nb_col):
				btn=Button(self.grille,background=btn_MORT,command=self.changeEtat(i,j),width=btn_width,height=btn_height)
				btn.grid(row=i,column=j)
				uneLigne.append(btn)
			self.btns.append(uneLigne)
		self.grille.pack(side='left')

		#Création des entrées pour la grille
		label_ligne=Label(self.entryGrille,text='Nombre de lignes: ')
		label_ligne.grid(row=0,column=0)
		self.entry_ligne=Entry(self.entryGrille)
		self.entry_ligne.grid(row=0,column=1)

		label_colonne=Label(self.entryGrille,text='Nombre de colonnes: ')
		label_colonne.grid(row=1,column=0)
		self.entry_colonne=Entry(self.entryGrille)
		self.entry_colonne.grid(row=1,column=1)

		self.btn_entry=Button(self.entryGrille,text='Modifier \nla taille de la grille: ',command=self.tailleGrille)
		self.btn_entry.grid(row=2,column=0)

		self.entryGrille.pack()

		#Création de l'entrée pour les cases vivante aléatoire
		label_alea=Label(self.alea,text='Nombre de cases vivante: ')
		label_alea.grid(row=0,column=0)
		self.entry_alea=Entry(self.alea)
		self.entry_alea.grid(row=0,column=1)

		self.btn_alea=Button(self.alea,text='Place les cases vivante',command=self.placeAlea)
		self.btn_alea.grid(row=1,column=0)

		self.alea.pack()

		#Création des boutons de commande
		self.btn_start=Button(self.cmd,text='Start',command=self.joue)
		self.btn_start.pack(side='left')

		self.btn_stop=Button(self.cmd,text='Stop',command=self.stop)
		self.btn_stop.pack(side='left')

		self.btn_clear=Button(self.cmd,text='Clear',command=self.clear)
		self.btn_clear.pack(side='left')

		self.cmd.pack()

		#Message en cas de boucle
		self.label_boucle=Label(self.fen)
		self.label_boucle.pack()

		#Boucle
		self.fen.mainloop()


	def changeEtat(self,i,j):
		"""VueJeuDeLaVie, int, int -> None

		Change la valeur du bouton en mort si il est vivant et inversement"""

		def traitement():

			if self.btns[i][j]['background']==btn_VIVANT:
				self.btns[i][j]['background']=btn_MORT
				self.jeu.grille[i][j]=MORT

			else:
				self.btns[i][j]['background']=btn_VIVANT
				self.jeu.grille[i][j]=VIVANT

		return traitement


	def tailleGrille(self):
		"""VueJeuDeLaVie -> None

		Modifie la taille de la grille"""

		#Récupère les nombres de lignes et de colonnes
		self.nb_lig=int(self.entry_ligne.get())
		self.nb_col=int(self.entry_colonne.get())

		#Construit un nouveau jeu aux bonnes dimensions
		self.jeu=JeuDeLaVie(self.nb_lig,self.nb_col)

		#Détruit et construit la nouvelle grille
		self.grille.destroy()

		self.grille=Frame(self.fen)
		self.btns=[]
		for i in range(self.nb_lig):
			uneLigne=[]
			for j in range(self.nb_col):
				btn=Button(self.grille,background=btn_MORT,command=self.changeEtat(i,j),width=btn_width,height=btn_height)
				btn.grid(row=i,column=j)
				uneLigne.append(btn)
			self.btns.append(uneLigne)
		self.grille.pack(side='left')


	def placeAlea(self):
		"""VueJeuDeLaVie -> None

		Place aléatoirement le nombre de cases vivante"""

		nb=int(self.entry_alea.get())
		self.jeu.placeNCelsRandom(nb)
		self.modifieBtnGrille()


	def prepareJeu(self):
		"""VueJeuDeLaVie -> None

		Lorqu'on lance le jeu, tout le boutons et entrées sont désactivé 
		à l'exception du bouton stop"""

		self.entry_ligne['state']='disable'
		self.entry_colonne['state']='disable'
		self.entry_alea['state']='disable'

		for i in range(self.nb_lig):
			for j in range(self.nb_col):
				self.btns[i][j]['state']='disable'

		self.btn_entry['state']='disable'
		self.btn_alea['state']='disable'
		self.btn_start['state']='disable'
		self.btn_clear['state']='disable'


	def modifieBtnGrille(self):
		"""VueJeuDeLaVie -> None

		Modifie la couleur des boutons si ils sont mort ou vivant"""

		for i in range(self.nb_lig):
			for j in range(self.nb_col):
				if self.jeu.grille[i][j]==MORT:
					self.btns[i][j]['background']=btn_MORT
				else:
					self.btns[i][j]['background']=btn_VIVANT


	def etapeSuivante(self):
		"""VueJeuDeLaVie -> None

		Avance le jeu d'une étape et l'arrète si la grille est vide"""

		self.jeu.etapeSuivante()
		if self.jeu.boucle():
			self.label_boucle['text']='Attention, le jeu boucle !'
		if self.jeu.grilleVide():
			self.stop()
		self.modifieBtnGrille()


	def joue(self):
		"""VueJeuDeLaVie -> None

		Fonction principale qui fait avancer le jeu
		Si le jeu n'est pas stoppé, alors il continue
		Si on le lance pour la première fois, on fait appel à la fonction
		de préparation du jeu"""

		if self.boucle:
			if self.debut:
				self.prepareJeu()
				self.debut=False
			self.etapeSuivante()
			self.fen.after(DELAI,self.joue)

		else:
			self.boucle=True


	def stop(self):
		"""VueJeuDeLaVie -> None

		Fonction qui arrète le jeu et remet les boutons et entrées à leur état initial"""

		self.debut=True
		self.boucle=False

		self.entry_ligne['state']='normal'
		self.entry_colonne['state']='normal'
		self.entry_alea['state']='normal'

		for i in range(self.nb_lig):
			for j in range(self.nb_col):
				self.btns[i][j]['state']='normal'

		self.btn_entry['state']='normal'
		self.btn_alea['state']='normal'
		self.btn_start['state']='normal'
		self.btn_clear['state']='normal'

		self.label_boucle['text']=''


	def clear(self):
		"""VueJeuDeLaVie -> None

		Met les boutons de la grille à zéro et supprime le texte dans les entry"""

		for i in range(self.nb_lig):
			for j in range(self.nb_col):
				self.btns[i][j]['background']=btn_MORT
				self.btns[i][j]['state']='normal'
				self.jeu.grille[i][j]=MORT

		self.entry_ligne.delete(0,END)
		self.entry_colonne.delete(0,END)
		self.entry_alea.delete(0,END)