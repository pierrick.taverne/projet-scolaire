# -*- coding: utf-8 -*-
#TAVERNE Pierrick

#Import de bibliothèque
import random

#Constantes
MORT='-'
VIVANT='*'
NB_ETAPE_BOUCLE=2


#Classe du jeu
class JeuDeLaVie:

	#Constructeur
	def __init__(self,nb_lig=10,nb_col=10):

		self.nb_lig=nb_lig
		self.nb_col=nb_col

		self.historique=[]

		#Création de la grille
		self.grille=[]
		for i in range(self.nb_lig):
			ligne=[]
			for j in range(self.nb_col):
				ligne.append(MORT)
			self.grille.append(ligne)


	def placeNCelsRandom(self,n):
		"""JeuDeLaVie, int -> None

		Place de manière aléatoire n cases vivante sur la grille"""

		newGrille=[]

		#Choix des n cases vivante
		listeVivant=[]
		while len(listeVivant)<n:
			x=(random.randint(0,self.nb_lig-1),random.randint(0,self.nb_col-1))
			if x not in listeVivant:
				listeVivant.append(x)

		#Mise à jour de la grille avec les cases vivante
		for i in range(self.nb_lig):
			ligne=[]
			for j in range(self.nb_col):
				if (i,j) in listeVivant:
					ligne.append(VIVANT)
				else:
					ligne.append(MORT)
			newGrille.append(ligne)
		self.grille=newGrille


	def grilleVide(self):
		"""JeuDeLaVie -> bool

		Retourne True si la grille ne contient pas de case vivante, False sinon"""

		for i in range(self.nb_lig):
			for j in range(self.nb_col):
				if self.grille[i][j]==VIVANT:
					return False
		return True


	def afficherGrille(self):
		"""JeuDeLaVie -> None

		Affiche la grille du jeu"""

		premiereLigne='Colonne  '
		for k in range(self.nb_col):
			premiereLigne+=str(k)+'  '
		print(premiereLigne)

		for i in range(self.nb_lig):
			uneLigne='Ligne '+str(i)+'  '
			for j in range(self.nb_col):
				uneLigne+=self.grille[i][j]+'  '
			print(uneLigne)


	def getAdj(self,i,j):
		"""JeuDeLaVie, int, int -> int

		Retourne le nombre de cases voisine vivante de la case (i,j)"""

		nb=0

		if i>0:
			if self.grille[i-1][j]==VIVANT:
				nb+=1
			if j>0 and self.grille[i-1][j-1]==VIVANT:
				nb+=1
			if j<self.nb_col-1 and self.grille[i-1][j+1]==VIVANT:
				nb+=1

		if i<self.nb_lig-1:
			if self.grille[i+1][j]==VIVANT:
				nb+=1
			if j>0 and self.grille[i+1][j-1]==VIVANT:
				nb+=1
			if j<self.nb_col-1 and self.grille[i+1][j+1]==VIVANT:
				nb+=1

		if j>0 and self.grille[i][j-1]==VIVANT:
			nb+=1

		if j<self.nb_col-1 and self.grille[i][j+1]==VIVANT:
			nb+=1

		return nb


	def etapeSuivante(self):
		"""JeuDeLaVie -> None

		Modifie la grille selon les règles du jeu"""

		newGrille=[]
		for i in range(self.nb_lig):
			uneLigne=[]
			for j in range(self.nb_col):
				nb=self.getAdj(i,j)
				if (self.grille[i][j]==MORT and nb==3) or (self.grille[i][j]==VIVANT and (nb==2 or nb==3)):
					uneLigne.append(VIVANT)
				else:
					uneLigne.append(MORT)
			newGrille.append(uneLigne)

		if len(self.historique)==NB_ETAPE_BOUCLE:
			del(self.historique[0])
		self.historique.append(self.grille)
		self.grille=newGrille


	def boucle(self):
		"""JeuDeLaVie -> bool

		Retourne si le jeu boucle à une ou deux étapes"""
		return self.historique[0]==self.grille











#Fonction principale du jeu

def main():

	#Initialisation du jeu
	print('Bienvenue dans le jeu de la vie')

	nb_lig=int(input('Entrez le nombre de lignes: '))
	nb_col=int(input('Entrez le nombre de colonnes: '))
	jeu=JeuDeLaVie(nb_lig,nb_col)

	nb_vivant=int(input('Entrez le nombre de cellules vivante: '))
	jeu.placeNCelsRandom(nb_vivant)

	print('\n\n')

	jeu.afficherGrille()

	choix=int(input('\nContinuer ?\n1: Oui\n0: Non\n'))

	#Boucle du jeu
	while True:
		print('\n\n')

		jeu.etapeSuivante()
		jeu.afficherGrille()

		if jeu.boucle():
			print('Attention le jeu boucle !')

		if jeu.grilleVide():
			return None

		choix=int(input('\nContinuer ?\n1: Oui\n0: Non\n'))
		
		if choix==0:
			return None