# -*- coding: utf-8 -*-

#Algo2
#Projet

#TAVERNE Pierrick
#LEGRAS Stephen

#importation des modules
import vue
import modele
import time

class Controleur:
    '''Controle le jeu Tetris'''

    def __init__(self,modele):
        '''Controleur, ModeleTetris -> Controleur

        Constructeur du controleur
        '''
        #On recupere le modele
        self.__modele=modele
        #On crée la vue
        self.__vue=vue.VueTetris(modele)
        #On recupere la fenetre
        self.__fen=self.__vue.fenetre()

        #on fait le lien avec les déplacements a gauche et a droite
        self.__fen.bind("<Key-Left>",self.forme_a_gauche)
        self.__fen.bind("<Key-Right>",self.forme_a_droite)
        #on fait le lien avec la touche bas
        self.__fen.bind("<Key-Down>",self.forme_tombe)
        #on fait le lien avec la touche haut
        self.__fen.bind("<Key-Up>",self.forme_tourne)
        #On fait le lien avec la touche a
        self.__fen.bind("<Key-a>",self.forme_change)

        #Variable pour la touche a
        self.__a=0

        #Demande à la vue de dessiner la forme du modele
        coords=self.__modele.get_coord_abs()
        couleur=self.__modele.get_couleur_forme()
        self.__vue.dessine_forme(coords,couleur)
        
        #Demande à la vue de dessiner la forme suivante
        coords_suiv=self.__modele.get_coords_suivante()
        couleur_suiv=self.__modele.get_couleur_suivante()
        self.__vue.dessine_forme_suivante(coords_suiv,couleur_suiv)

        #Delai avec lequel la forme tombe
        self.__delai=320
        #Fonction qui fait tomber la forme
        self.joue()

        #Boucle principale
        self.__fen.mainloop()

    def joue(self):
        '''Controleur -> None

        boucle principale du jeu
        Fait tomber une forme d'une ligne
        '''
        #Tant que ce n'est pas fini
        if not self.__modele.fini():
            #On met a jour la  forme et le terrain
            self.affichage()
            #On rappel la méthode joue au bout du delai
            self.__fen.after(self.__delai,self.joue)

    def affichage(self):
        '''Controleur -> None

        Indique au module de faire tomber la forme
        puis demande à la vue de redessiner son terrain et la forme
        '''
        #On fait tomber la forme
        collision = self.__modele.forme_tombe()
        #on verifie si il ya eu collision et donc si la forme s'est
        #posée sur le terrain on remet le delai a sa valeur initiale
        if collision:
            self.__delai = 320
            #On redessine la forme suivante
            coords_suiv=self.__modele.get_coords_suivante()
            couleur_suiv=self.__modele.get_couleur_suivante()
            self.__vue.dessine_forme_suivante(coords_suiv,couleur_suiv)

        #On redessine le terrain et la forme
        self.__vue.dessine_terrain()
        coords=self.__modele.get_coord_abs()
        couleur=self.__modele.get_couleur_forme()
        self.__vue.dessine_forme(coords,couleur)
        val=self.__modele.get_score()
        self.__vue.met_a_jour_score(val)

    def forme_a_gauche(self,event):
        '''
        Controleur, event -> none

        demande au modéle de deplacer la forme a gauche
        '''
        self.__modele.forme_a_gauche()

    def forme_a_droite(self,event):
        '''
        Controleur, event -> none

        demande au modéle de deplacer la forme a droite
        '''
        self.__modele.forme_a_droite()

    def forme_tombe(self,event):
        '''
        Controleur, event -> None

        modifie la valeur de delai
        '''
        self.__delai = 100

    def forme_tourne(self,event):
        '''
        Controleur,event -> none

        demande au modele de faire tourner la forme
        '''
        self.__modele.forme_tourne()

    def forme_change(self,event):
        '''
        Controleur,event -> None
        
        Change la forme qui tombe avec les coordonnées
        de la forme suivante selon la liste des formes
        '''
        if self.__a<10:
            self.__modele.forme_change()
            self.__a+=1








#Script principal
if __name__=="__main__":
    #Création du modèle
    tetris=modele.ModeleTetris()
    #Création du contrôleur qui cré la vue
    #et lance la boucle d'écoute des évts
    ctrl=Controleur(tetris)
