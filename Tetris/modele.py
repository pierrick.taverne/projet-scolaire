# -*- coding: utf-8 -*-

#Algo2
#Projet

#TAVERNE Pierrick
#LEGRAS Stephen

#importation de randint
from random import randint

#Constante
LES_FORMES=[
            [(-1,0),(0,0),(1,0),(2,0)],
            [(0,0),(1,0),(1,1),(0,1)],
            [(-1,1),(0,1),(0,0),(1,0)],
            [(-1,0),(0,0),(0,1),(1,1)],
            [(-1,1),(-1,0),(0,0),(1,0)],
            [(-1,0),(0,0),(1,0),(1,1)],
            [(-1,0),(0,0),(1,0),(0,1)]
            ]

class ModeleTetris:

    def __init__(self,lig=20,col=10):
        '''
        ModeleTetris, int,int -> ModeleTetris

        modelise le terrain du tetris
        '''
        self.__haut = 4 + lig
        self.__larg =  col
        self.__base = 4

        #creation de le partie grise du terrain
        self.__terrain=[]
        for i in range(self.__base):
            nv_lig=[]
            for j in range(self.__larg):
                nv_lig.append(-2)
            self.__terrain.append(nv_lig)

        #creation de la partie noir du terrain
        for i in range(lig):
            nv_lig=[]
            for j in range(self.__larg):
                nv_lig.append(-1)
            self.__terrain.append(nv_lig)

        self.__forme = Forme(self)
        self.__suivante = Forme(self)
        self.__score = 0

    def get_largeur(self):
        '''
        ModeleTetris -> int

        retourne le nombre de colonnes du tetris
        '''
        return self.__larg

    def get_hauteur(self):
        '''
        ModeleTetris -> int

        retourne le nombre de ligne du tetris
        '''
        return self.__haut

    def get_valeur(self,i,j):
        '''
        ModeleTetris, int,int -> int

        retourne le valeur de la case (i,j) du terrain
        '''
        return self.__terrain[i][j]

    def est_occupe(self,i,j):
        '''
        ModeleTetris, int,int -> bool

        retourne un bool qui indique si
        la case (i,j) du terrain est occupé
        '''
        return self.__terrain[j][i] >= 0

    def fini(self):
        '''
        ModeleTetris -> bool

        retourne un bool qui indique si la partie est fini ou non
        '''
        j = self.__base
        for i in range(self.__larg):
            #pour chaque case de la ligne base
            # on vérifie si une case est diffèrente de -1
            if self.est_occupe(i,j):
                return True
        return False

    def ajoute_forme(self):
        '''
        ModeleTetris -> ModeleTetris

        pose la forme sur le terrain
        '''
        coord_forme = self.get_coord_abs()
        #on recupère les coordonnées absolues
        #elles sont une liste de liste

        #pour chaque case de la forme
        for i in range(len(coord_forme)):
            self.__terrain[coord_forme[i][1]][coord_forme[i][0]] = self.get_couleur_forme()
            #les colonne et ligne sont inversé dans la forme et le modele
            #tetris donc c'est pour cela que l'on a mis [1] pour la ligne
            #et [0] pour la colonne

    def forme_tombe(self):
        '''
        ModeleTetris -> bool

        fait tomber la forme, si il y a collision ajoute la forme sur
        le terrain et retourne un bool si il ya eu collision ou non
        '''
        self.supprime_ligne_complete()
        collision = self.__forme.tombe()
        if collision :
            self.ajoute_forme()
            #on ajoute une la forme sur le terrain
            #on en crée une nouvelle
            self.__forme = Forme(self)
            self.__forme = self.__suivante
            self.__suivante = Forme(self)
            return True
        else:
            return False

    def get_couleur_forme(self):
        '''
        ModeleTetris -> int

        retourne la couleur de la forme
        '''
        return self.__forme.get_couleur()

    def get_couleur_suivante(self):
        '''ModeleTetris -> int

        retourne la couleur de __suivante
        '''
        return self.__suivante.get_couleur()

    def get_coord_abs(self):
        '''
        ModeleTetris -> list(list(int))

        retourne les coordonnées absolue de la forme
        '''
        return self.__forme.get_coords()

    def get_coords_suivante(self):
        '''ModeleTetris -> list(int,int)

        retourne les coordonnées relatives de __suivante
        '''
        return self.__suivante.get_coords_relatives()

    def forme_a_gauche(self):
        '''
        ModeleTetris -> None

        demande a __forme de se deplacer a gauche
        '''
        self.__forme.a_gauche()

    def forme_a_droite(self):
        '''
        ModeleTetris -> None

        demande a __forme de se deplacer a droite
        '''
        self.__forme.a_droite()

    def forme_tourne(self):
        '''
        ModeleTetris -> None

        demande a la forme de tourner
        '''
        self.__forme.tourne()

    def est_ligne_complete(self,lig):
        '''
        ModeleTetris, int -> bool

        retourne True si la ligne lig est compléte
        '''
        for j in range(self.__larg):
            if not self.est_occupe(j,lig):
                return False
        return True

    def supprime_ligne(self,lig):
        '''
        ModeleTetris, int  -> None

        supprime la ligne d'indice lig et fait descendre les ligne
        supérieures.
        '''
        for i in range(0,lig - self.__base):
            for j in range(self.__larg):
                self.__terrain[lig-i][j] = self.__terrain[lig-i-1][j]
        for i in range(self.__larg):
            self.__terrain[self.__base][i] = -1

    def supprime_ligne_complete(self):
        '''
        ModeleTetris -> None

        supprime tout les lignes complete du terrain
        '''
        for i in range(self.__base,self.__haut):
            if self.est_ligne_complete(i):
                self.supprime_ligne(i)
                self.__score += 1

    def get_score(self):
        '''
        ModeleTetris -> int

        retourne la valeur du score
        '''
        return self.__score

    def forme_change(self):
        '''
        ModeleTetris -> None
        
        Demande a la forme de changer
        '''
        self.__forme.change()

class Forme:
    '''Modelise les formes du Tetris'''

    def __init__(self,modele):
        '''Forme, ModeleTetris -> Forme

        Constructeur de la class Forme
        '''
        #Création des attributs principaux
        self.__modele=modele
        self.__ind_forme=randint(0,len(LES_FORMES)-1)
        self.__couleur=self.__ind_forme
        self.__forme=LES_FORMES[self.__ind_forme]
        self.__y0=0
        #x0 en fonction de la forme
        if self.__ind_forme==0:
            self.__x0=randint(1,self.__modele.get_largeur()-3)
        elif self.__ind_forme==1:
            self.__x0=randint(0,self.__modele.get_largeur()-2)
        else:
            self.__x0=randint(1,self.__modele.get_largeur()-2)

    def get_couleur(self):
        '''Forme -> int

        retourne la couleur de la forme
        '''
        return self.__couleur

    def get_coords(self):
        '''Forme -> list(list(int,int))

        retourne les coordonnées absolues de la forme sur le terrain
        '''
        #Création de la liste
        coord_abs=[]

        #Pour chaque coordonnées de la forme
        #on crée un couple avec les coordonnées absolues
        #et on ajoute ce couple à notre nouvelle liste
        for x,y in self.__forme:
            couple = (x + self.__x0, y + self.__y0)
            coord_abs.append(couple)
        return coord_abs

    def get_coords_relatives(self):
        '''Forme -> list(int,int)

        retourne une copie de la liste des coordonnées relatives de la forme
        '''
        coord=self.__forme[:]
        return coord

    def collision(self):
        '''Forme -> bool

        retourne True si la forme doit se poser sinon False
        '''
        #On recupere le ModeleTetris
        modele=self.__modele

        #On recupere les coordonnées absolues de la forme
        coord_abs=self.get_coords()

        #On verifie pour chaque coordonnées si il se situe à la derniere ligne
        for i in range(len(coord_abs)):
            if coord_abs[i][1]==modele.get_hauteur()-1:
                return True
        #On verifie si la forme se situe juste au dessus d'une autre
        for i in range(len(coord_abs)):
            if modele.est_occupe(coord_abs[i][0],coord_abs[i][1]+1):
                return True


        #On trouve aucune collision on retourne False
        return False

    def tombe(self):
        '''Forme -> bool

        fait tomber la forme d'une ligne
        '''
        #Si il n'y a pas de collision
        #on descend la forme d'une ligne
        #pour ça on augmente la valeur de y0
        #et on retourne False
        if not self.collision():
            self.__y0+=1
            return False
        #sinon on retourne True
        return True

    def position_valide(self):
        '''Forme -> bool

        teste si chaque coordonnée absolue (x,y) de la forme est valide
        '''
        #On recupere les coordonnées absolues
        coord_abs=self.get_coords()

        #Pour chaque couple (x,y) de la forme
        for i in range(len(coord_abs)):
            #On verifie la position de x
            if coord_abs[i][0]<0 or coord_abs[i][0]>=self.__modele.get_largeur():
                return False
            #la position de y
            if coord_abs[i][1]<0 or coord_abs[i][1]>=self.__modele.get_hauteur():
                return False
            #si la case en (x,y) n'est pas occupe
            if self.__modele.est_occupe(coord_abs[i][0],coord_abs[i][1]):
                return False
        return True

    def a_gauche(self):
        '''Forme -> None

        Deplace la forme a gauche si c'est possible
        '''
        self.__x0-=1
        if not self.position_valide():
            self.__x0+=1

    def a_droite(self):
        '''Forme -> None

        Deplace la forme a droite si c'est possible
        '''
        self.__x0+=1
        if not self.position_valide():
            self.__x0-=1

    def tourne(self):
        '''
        Forme -> None

        tourne une forme
        '''
        forme_prec = self.__forme
        self.__forme=[]
        #on remet la forme a une liste vide
        #pour chaque couple de forme_prec
        for couple in forme_prec:
            #on inverse les coordonnees
            self.__forme.append((-couple[1],couple[0]))
        if not self.position_valide() :
            #on verifie si la forme n'est pas valide
            # si elle ne l'est pas on remet la forme a sa forme_prec
            self.__forme = forme_prec

    def change(self):
        '''
        Forme -> None
        
        La forme change pour prendre les coordonnees
        de la forme suivante dans la liste LES_FORMES
        '''
        #On prend les forme suivante en ajoutant 1
        self.__ind_forme+=1
        #Si on sort de la liste on revient a 0
        if self.__ind_forme>=len(LES_FORMES):
            self.__ind_forme=0
        #On modifie la forme
        self.__couleur=self.__ind_forme
        self.__forme=LES_FORMES[self.__ind_forme]
