# -*- coding: utf-8 -*-

#Algo2
#Projet

#TAVERNE Pierrick
#LEGRAS Stephen

#importation des modules
import tkinter
from random import randint

#Constante
DIM = 21 #taille d'une case
COULEURS=['red',
        'blue',
        'green',
        'yellow',
        'orange',
        'purple',
        'pink',
        'dark grey',
        'black']

SUIVANT = 6

class VueTetris:

    def __init__(self, modele):
        '''
        VueTetris, ModeleTetris --> VueTetris

        modelise la vue du tetris
        '''
        self.__modele = modele
        #fenetre
        self.__fen=tkinter.Tk()
        self.__fen.title('Tetris - Projet Algo')

        self.__nblig = modele.get_hauteur()
        self.__nbcol = modele.get_largeur()

        #creation du canvas du terrain
        self.__can_terrain = tkinter.Canvas(self.__fen,
                                width = self.__nbcol * DIM,
                                height= self.__nblig * DIM)
        self.__les_cases = []
        for i in range(self.__nblig):
            une_ligne=[]
            for j in range(self.__nbcol):
                #on recupere la valeur de la case
                coul = modele.get_valeur(i,j)
                case = self.__can_terrain.create_rectangle(j*DIM+1,i*DIM+1,(j+1)*DIM,(i+1)*DIM,
                                                        outline='white',
                                                        fill=COULEURS[coul])
                une_ligne.append(case)
            self.__les_cases.append(une_ligne)

        self.__can_terrain.pack(side='left')

        #forme suivante
        #label forme suivante
        lbl_suiv = tkinter.Label(self.__fen,text='Forme suivante')
        #creattion du canvas
        self.__can_fsuivante = tkinter.Canvas(self.__fen,
                                            width=SUIVANT * DIM,
                                            height=SUIVANT * DIM)
        #creation des cases
        self.__les_suivants = []
        for i in range(SUIVANT):
            une_ligne=[]
            for j in range(SUIVANT):
                case = self.__can_fsuivante.create_rectangle(j*DIM+1,i*DIM+1,(j+1)*DIM,(i+1)*DIM,
                                                        outline='white',
                                                        fill='black')
                une_ligne.append(case)
            self.__les_suivants.append(une_ligne)
        self.__can_fsuivante.pack()

        #Label pour l'affichage du score
        self.__lbl_score=tkinter.Label(self.__fen,text='Score: 0')
        self.__lbl_score.pack()

        #creation du bouton quitter
        btn_quit = tkinter.Button(self.__fen,
                    text='Quitter',
                    command = self.__fen.destroy)
        btn_quit.pack(side='right')

        #creation des boutons sur le coté

        frame = tkinter.Frame(self.__fen)
        #self.__lbl_score=tkinter.Frame(frame)
        btn_quit = tkinter.Frame(frame)
        frame.pack(side='right')



    def fenetre(self):
        '''
        VueTetris -> tkinter

        retourne l'instance de tk
        '''
        return self.__fen

    def dessine_case(self,lig,col,coul):
        '''
        VueTetris, int,int,int -> None

        change la couleur de la case au coordonnées [i,j]
        en remplaçant par coul
        '''
        self.__can_terrain.itemconfigure(self.__les_cases[lig][col],
                                    fill=COULEURS[coul])

    def dessine_case_suivante(self,lig,col,coul):
        '''
        VueTetris, int,int,int -> None

        change la couleur de la case du canvas de la forme suivante
        au coordonnées [i,j] en remplaçant par coul
        '''
        self.__can_fsuivante.itemconfigure(self.__les_suivants[lig][col],
                                    fill=COULEURS[coul])

    def dessine_terrain(self):
        '''
        VueTetris -> VueTetris

        met à jour la couleur de tout le terrain
        '''
        for i in range(self.__nblig):
            for j in range(self.__nbcol):
                coul = self.__modele.get_valeur(i,j)
                self.dessine_case(i,j,coul)

    def dessine_forme(self,coords,couleur):
        '''
        VueTetris, list(int,int), int -> None

        fait apparaitre une forme sur le terrain aux
        coordonnés x,y de la COULEURS d'indice couleur
        '''
        for x,y in coords:
            self.dessine_case(y,x,couleur)

    def dessine_forme_suivante(self,coords,couleur):
        '''
        VueTetris, list(int,int), int -> None

        fait apparaitre la forme suivante sur le canvas aux
        coordonnés x,y de la COULEURS d'indice couleur
        '''
        self.nettoie_forme_suivante()
        for x,y in coords:
            self.dessine_case_suivante(y+2,x+2,couleur)

    def met_a_jour_score(self,val):
        '''
        VueTetris, int -> None

        Met a jour l'affichage du score
        '''
        self.__lbl_score["text"]='Score: '+str(val)

    def nettoie_forme_suivante(self):
        '''
        VueTetris -> none

        remet du noir sur le case du canvas de la forme suivante
        '''
        for i in range(SUIVANT):
            for j in range(SUIVANT):
                self.dessine_case_suivante(i,j,-1)
