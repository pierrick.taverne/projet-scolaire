import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Othellier est la classe representant le plateau du jeu Othello et les affichages utile 
 *  comme le joueur qui doit jouer, les scores des joueurs ou encore le chrono.
 * 
 * @see Case
 * @see Chrono
 *
 * @author TAVERNE Pierrick
 * @version 1.0
 */

public class Othellier extends JPanel{
	private static final int taille = 8;
	private static final int[] N = {-1,0};
	private static final int[] NE = {-1,1};
	private static final int[] E = {0,1};
	private static final int[] SE = {1,1};
	private static final int[] S = {1,0};
	private static final int[] SW = {1,-1};
	private static final int[] W = {0,-1};
	private static final int[] NW = {-1,-1};

	private int mode;
	private int joueur;

	private Case[][] board;

	private Case[] possible;
	private int nbPossible;

	private JLabel labelJoueur;
	private JLabel labelChrono;
	private JLabel labelScoreNoir;
	private JLabel labelScoreBlanc;

	private int scoreNoir;
	private int scoreBlanc;

	private boolean finChrono;
	private Chrono chrono;

	/**
	 * L'instanciation construit le plateau de cases et initialise plusieur attributs comme les scores, 
	 *  le chronometre, etc ...
	 *
	 * @see Case
	 * @see Chrono
	 * 
	 * @param n
	 * 			Mode de jeu:
	 *			<ul>
	 *			 <li>1: Un joueur</li>
	 *			 <li>2: Deux joueurs</li>
	 *			</ul>
	 */
	public Othellier(int n){
		this.mode = n;
		this.joueur = 1;

		this.setLayout(new GridLayout(this.taille+1,this.taille));
		this.board = new Case[this.taille][this.taille];
		for (int i = 0; i < this.taille; i++){
			for (int j = 0; j < this.taille; j++){
				this.board[i][j] = new Case(i,j);
				this.add(this.board[i][j].getBtn());
			}
		}

		this.possible = new Case[64];
		this.nbPossible = 0;

		this.labelJoueur = new JLabel("Joueur: noir");
		add(labelJoueur);
		this.labelScoreNoir = new JLabel("Score joueur noir: " + this.scoreNoir);
		add(labelScoreNoir);
		this.labelScoreBlanc = new JLabel("Score joueur blanc: " + this.scoreBlanc);
		add(labelScoreBlanc);

		this.scoreNoir = 2;
		this.scoreBlanc = 2;

		this.finChrono = false;
		this.chrono = new Chrono();
		Timer t = new Timer();
		t.schedule(new TimerTask(){
			public void run(){
				majChrono();
			}
		},1000,1000);

		this.labelChrono = new JLabel(this.chrono.get());
		add(labelChrono);
	}

	/**
	 * Retourne la taille du plateau.
	 *
	 * @return Taille de l'Othellier.
	 */
	public int getTaille(){
		return this.taille;
	}

	/**
	 * Retourne le plateau.
	 *
	 * @return Le tableau a deux dimensions contenant les cases.
	 */
	public Case[][] getBoard(){
		return this.board;
	}

	/**
	 * Retourne une case du plateau.
	 *
	 * @param x
	 *			Ordonnee de la case.
	 * @param y
	 *			Abscisse de la case.
	 *
	 * @return La case aux coordonnees (y,x).
	 */
	public Case getCase(int x, int y){
		return this.board[x][y];
	}

	/**
	 * Retourne le plateau en String.
	 * 
	 * @return Le plateau de cases sous forme d'une chaine de caracteres.
	 */
	public String toString(){
		String res = " |";

		for (int i = 0; i < this.taille; i++){
			res += (char)('a'+i);
			res += '|';
		}

		res += "\n";

		for (int i = 0; i < this.taille; i++){
			res += (char)(49+i);
			res += '|';

			for (int j = 0; j < this.taille; j++){
				if (this.board[i][j].getValeur() == 1) res += 'X';
				else if (this.board[i][j].getValeur() == 2) res += 'O';
				else res += ' ';
				res += '|';
			}

			res += "\n";
		}

		return res;
	}

	/**
	 * Affiche l'image correspondant au joueur qui joue et modifie la valeur de la case.
	 *
	 * @see Case#setValeur
	 *
	 * @param x
	 *			Ordonnee de la case a retourne.
	 * @param y
	 *			Abscisse de la case a retourne.
	 */
	public void retourneCase(int x, int y){
		this.board[x][y].getBtn().setIcon(Case.TABIMAGES[this.joueur]);
		this.board[x][y].setValeur(this.joueur);
	}

	/**
	 * Cette methode cherche les cases disponible pour l'IA et joue avec la premiere trouve.
	 *
	 * @see Othellier#positionPossible
	 *
	 * @see Othellier#joue
	 */
	public void joueIA(){
		positionPossible(0);
		joue(this.possible[0].getX(),this.possible[0].getY());
	}

	/**
	 * Cette methode commence en premier par vider le tableau des positions possible.<br/>
	 * Elle regarde ensuite dans toutes les directions si il y a des changements possible avec nbChangement, 
	 *  et si c'est le cas elle retourne les cases avec retourneCase.<br/>
	 * Enfin elle fait appel a changeJoueur et a score pour les mettre a jour.
	 * 
	 * @see Othellier#nbChangement
	 *
	 * @see Othellier#retourneCase
	 *
	 * @see Othellier#changeJoueur
	 *
	 * @see Othellier#score
	 *
	 * @param x
	 *			Ordonnee de la case jouee.
	 * @param y
	 *			Abscisse de la case jouee.
	 */
	public void joue(int x, int y){
		while (this.nbPossible > 0){
			this.nbPossible--;
			this.possible[this.nbPossible].getBtn().setIcon(Case.TABIMAGES[0]);
			this.possible[this.nbPossible] = null;
		}
		if (this.board[x][y].getValeur() == 0){
			boolean fait = false;
			int i;
			int n;
			if ((n = nbChangement(x,y,this.N)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.N[0]),y+(i*this.N[1]));
				}
			}
			if ((n = nbChangement(x,y,this.NE)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.NE[0]),y+(i*this.NE[1]));
				}
			}
			if ((n = nbChangement(x,y,this.E)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.E[0]),y+(i*this.E[1]));
				}
			}
			if ((n = nbChangement(x,y,this.SE)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.SE[0]),y+(i*this.SE[1]));
				}
			}
			if ((n = nbChangement(x,y,this.S)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.S[0]),y+(i*this.S[1]));
				}
			}
			if ((n = nbChangement(x,y,this.SW)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.SW[0]),y+(i*this.SW[1]));
				}
			}
			if ((n = nbChangement(x,y,this.W)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.W[0]),y+(i*this.W[1]));
				}
			}
			if ((n = nbChangement(x,y,this.NW)) != 0){
				if (fait == false) i = 0;
				else i = 1;
				fait = true;
				for (; i <= n; i++){
					retourneCase(x+(i*this.NW[0]),y+(i*this.NW[1]));
				}
			}
			if (fait){
				if (this.joueur == 1){
					this.joueur = 2;
				}
				else{
					this.joueur = 1;
				}
				changeJoueur();
			}
		}
		System.out.println(toString());
		score();
	}

	/**
	 * positionPossible enregistre toutes les cases ou le joueur peut jouer dans le tableau possible.
	 *  Le nombre de cases est lui enregistre dans nbPossible.<br/>
	 * De plus si le parametre v == 1, on retourne la case en vert pour la montrer au joueur.<br/>
	 * v == 0, lorsuq'on veut juste avoir la liste des cases disponible (comme pour faire jouer l'IA).
	 *
	 * @see Othellier#nbChangement
	 * 
	 * @param v
	 *			Valeur pour savoir si il faut retourner la case
	 *
	 * @return Le nombre de case ou il est possible de jouer pour le joueur.
	 */
	public int positionPossible(int v){
		for (int x = 0; x < this.taille; x++){
			for (int y = 0; y < this.taille; y++){
				if (this.board[x][y].getValeur() == 0){
					if (nbChangement(x,y,this.N) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.NE) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.E) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.SE) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.S) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.SW) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.W) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
					else if (nbChangement(x,y,this.NW) != 0){
						this.possible[this.nbPossible] = this.board[x][y];
						this.nbPossible++;
						if (v == 1){
							this.board[x][y].getBtn().setIcon(Case.TABIMAGES[3]);
						}
					}
				}
			}
		}
		return this.nbPossible;
	}

	/**
	 * Cette methode compte et retourne le nombre de case qu'il est possible de capturer avec la case (y,x) dans la direction d.<br/>
	 * d est un des parametre static de cette classe.
	 *
	 * @param x
	 *			Ordonnee de la case.
	 * @param y
	 *			Abscisse de la case.
	 * @param d
	 *			Direction dans laquelle on veut compter le nombre de case.
	 *
	 * @return Le nombre de case que l'on peut changer dans cette direction.
	 */
	public int nbChangement(int x, int y, int[] d){
		int i = x+d[0];
		int j = y+d[1];
		int nb = 0;

		while (i >= 0 && i < this.taille && j >= 0 && j < this.taille && this.board[i][j].getValeur() != this.joueur && this.board[i][j].getValeur() != 0){
			nb++;
			i += d[0];
			j += d[1];
		}

		if (i < 0 || i >= taille || j < 0 || j >= taille) return 0;
		else if (nb != 0 && this.board[i][j].getValeur() == this.joueur) return nb;
		else return 0;
	}

	/**
	 * Verifie que le joueur actuel peut jouer. Si ce n'est pas le cas elle change de joueur.<br/>
	 * Si aucun des deux joueurs ne peut jouer elle arrete la partie.
	 *
	 * @see Othellier#positionPossible
	 *
	 * @see Othellier#joueIA
	 *
	 * @see Othellier#fin
	 */
	public void changeJoueur(){
		int n;

		n = positionPossible(0);
		while (nbPossible > 0){
			nbPossible--;
			possible[nbPossible] = null;
		}
		if (n != 0){
			if (this.mode == 1 && this.joueur == 2){
				Timer t = new Timer();
				t.schedule(new TimerTask(){
					public void run(){
						joueIA();
					}
				},2000);
			}
			return;
		}
		else if (n == 0 && this.joueur == 1) this.joueur = 2;
		else if (n == 0 && this.joueur == 2) this.joueur = 1;

		n = positionPossible(0);
		while (nbPossible > 0){
			nbPossible--;
			possible[nbPossible] = null;
		}
		if (n != 0){
			if (this.mode == 1 && this.joueur == 2){
				Timer t = new Timer();
				t.schedule(new TimerTask(){
					public void run(){
						joueIA();
					}
				},2000);
			}
			return;
		}
		else fin();
		return;
	}

	/**
	 * Met a jour le chronometre si il y a une partie en cours.
	 *
	 * @see Chrono#get
	 */
	public void majChrono(){
		if (this.finChrono == false) this.labelChrono.setText(this.chrono.get());
	}

	/**
	 * Met a jour les scores des joueurs et l'affichage des scores.
	 */
	public void score(){
		this.scoreNoir = 0;
		this.scoreBlanc = 0;

		for (int i = 0; i < this.taille; i++){
			for (int j = 0; j < this.taille; j++){
				int n = this.board[i][j].getValeur();
				if (n == 1) this.scoreNoir++;
				else if (n == 2) this.scoreBlanc++;
			}
		}

		this.labelScoreNoir.setText("Score joueur noir: " + this.scoreNoir);
		this.labelScoreBlanc.setText("Score joueur blanc: " + this.scoreBlanc);

		if (this.joueur == 2 && this.mode == 1) this.labelJoueur.setText("Joueur: IA");
		else if (this.joueur == 2 && this.mode == 2) this.labelJoueur.setText("Joueur: blanc");
		else this.labelJoueur.setText("Joueur: noir");
	}

	/**
	 * Calcule le score final de chaque joueur.<br/>
	 * Si a la fin de la partie il reste des cases neutre, ces case vont au gagnant et donc s'additionne a son score.
	 */
	public void scoreFinal(){
		if ((this.scoreBlanc + this.scoreNoir) != (this.taille * this.taille)){
			if (this.scoreNoir > this.scoreBlanc) this.scoreNoir += (this.taille * this.taille) - (this.scoreNoir + this.scoreBlanc);
			else this.scoreBlanc += (this.taille * this.taille) - (this.scoreNoir + this.scoreBlanc);

			this.labelScoreNoir.setText("Score joueur noir: " + this.scoreNoir);
			this.labelScoreBlanc.setText("Score joueur blanc: " + this.scoreBlanc);
		}
	}

	/**
	 * Cette methode arrete le chronometre, fait appel a la methode scoreFinal et ouvre une fenetre pop-up pour informer le joueur.
	 *
	 * @see Othellier#scoreFinal
	 */
	public void fin(){
		this.finChrono = true;
		scoreFinal();

		JOptionPane messFin = new JOptionPane();
		String res;

		if (this.scoreNoir > this.scoreBlanc) res = "Le joueur noir à gagné";
		else if (this.scoreNoir < this.scoreBlanc && this.mode == 1) res = "L'IA à gagné";
		else res = "Le joueur blanc à gagné";

		res += "\nScore: " + this.scoreNoir + "-" + this.scoreBlanc;
		messFin.showMessageDialog(null, res, "Fin de la partie", JOptionPane.INFORMATION_MESSAGE);
		
		System.out.println("Fin");
	}


/*
	public static void main(String args[]){
		Othellier o = new Othellier();
		System.out.println(o.toString());
	}
	*/
}