import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * MenuBar est la classe representant le menu du jeu Othello, il permet de choisir son mode de jeu, mais egalement d'avoir une aide lors du jeu.
 *
 * @see Game
 * 
 * @author TAVERNE Pierrick
 * @version 1.0
 */

public class MenuBar extends JMenuBar{

	/**
	 * Ce MenuBar est compose de:
	 * <ul>
	 *  <li>Un sous-menu pour commencer une nouvelle partie, compose de deux boutons:
	 *	 <ul>
	 *    <li>Mode: contre l'IA.</li>
	 *    <li>Mode: deux joueurs.</li>
	 *   </ul>
	 *  </li>
	 *  <li>Un bouton help qui permet d'aider le joueur.</li>
	 * </ul>
	 *
	 */
	public MenuBar(){
		JMenu jouer = new JMenu("Jouer");

		JMenuItem unJoueur = new JMenuItem("Un joueur");
		JMenuItem deuxJoueur = new JMenuItem("Deux joueur");

		jouer.add(unJoueur);
		jouer.add(deuxJoueur);
		
		JMenuItem help = new JMenuItem("Help");

		unJoueur.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Game g = (Game)MenuBar.this.getTopLevelAncestor();
				g.changeOthellier(1);
			}
		});

		deuxJoueur.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Game g = (Game)MenuBar.this.getTopLevelAncestor();
				g.changeOthellier(2);
			}
		});

		help.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Game g = (Game)MenuBar.this.getTopLevelAncestor();
				g.getOthellier().positionPossible(1);
			}
		});

		this.add(jouer);
		this.add(help);
	}
}