import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Case est la classe representant une case du plateau du jeu Othello.
 * 
 * @see Othellier
 *
 * @author TAVERNE Pierrick
 * @version 1.0
 */

public class Case{
	public static final ImageIcon[] TABIMAGES = {
		new ImageIcon("images/neutre.jpg"),
		new ImageIcon("images/noir.jpg"),
		new ImageIcon("images/blanc.jpg"),
		new ImageIcon("images/possible.jpg")
	};
	private JButton btn;
	private int x;
	private int y;
	private int valeur;

	/**
	 * A la construction d'une case, on lui donnne un abscisse et une ordonnee.<br/>
	 * On lui affecte egalement un bouton et une valeur.
	 * 
	 * @param x
	 * 			Ordonnee
	 * @param y
	 *			Abscisse
	 */
	public Case(int x, int y){
		this.x = x;
		this.y = y;

		if ((this.x == 3 && this.y == 3)||(this.x == 4 && this.y == 4)){
			this.btn = new JButton(TABIMAGES[2]);
			this.valeur = 2;
		}else if ((this.x == 4 && this.y == 3)||(this.x == 3 && this.y == 4)){
			this.btn = new JButton(TABIMAGES[1]);
			this.valeur = 1;
		}else{
			this.btn = new JButton(TABIMAGES[0]);
			this.valeur = 0;
		}
		
		this.btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Game g = (Game)Case.this.btn.getTopLevelAncestor();
				g.getOthellier().joue(Case.this.x,Case.this.y);
			}
		});
	}

	/**
	 * Retourne cette case en String.
	 * 
	 * @return L'ordonnee, l'abscisse et la valeur de cette case sous forme d'une chaine de caracteres.
	 */
	public String toString(){
		return "" + (char)(this.y+'a') + (char)(this.x+49) + ": " + this.valeur;
	}

	/**
	 * Retourne l'ordonnee de cette case.
	 * 
	 * @return L'ordonnee de cette case.
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * Retourne l'abscisse de cette case.
	 * 
	 * @return L'abscisse de cette case.
	 */
	public int getY(){
		return this.y;
	}

	/**
	 * Retourne la valeur de cette case.
	 * 
	 * @return La valeur de cette case.
	 */
	public int getValeur(){
		return this.valeur;
	}

	/**
	 * Met a jour la valeur de cette case.
	 * 
	 * @param val
	 *			La nouvelle valeur de cette case.
	 */
	public void setValeur(int val){
		this.valeur = val;
	}

	/**
	 * Retourne le bouton de cette case.
	 * 
	 * @return Le bouton de cette case.
	 */
	public JButton getBtn(){
		return this.btn;
	}

/*
	public static void main(String[] args){
		Case c1 = new Case(0,8);
		Case c2 = new Case(4,5,1);
		System.out.println(c1.toString());
		System.out.println(c2.toString());
		c1.setValeur(2);
		System.out.println(c1.toString());
	}
	*/
}