
/**
 * Chrono est la classe representant un chronometre, il permet uniquement d'etre initialiser lors de l'instanciation et d'obtenir le temps ecouler.
 * 
 * @see Chrono#Chrono()
 * @see Chrono#get()
 *
 * @author TAVERNE Pierrick
 * @version 1.0
 */

public class Chrono{
	private long tempsDepart;
	private long temps;
	private long duree;

	/**
	 * Lors de l'instanciation ce chrono enregistre l'heure.
	 */
	public Chrono(){
		this.tempsDepart = System.currentTimeMillis();
		this.temps = 0;
		this.duree = 0;
	}

	/**
	 * Retourne la duree enregistrer par ce chrono.
	 * 
	 * @return La duree ecoule depuis l'instanciation sous forme de chaine de caracteres.<br/>
	 * 			Resultat sous la forme: XXmin XXsec
	 */
	public String get(){
		this.temps = System.currentTimeMillis();
		this.duree = this.temps - this.tempsDepart;
		this.duree /= 1000;

		String res = "Chrono: ";

		long m = this.duree / 60;
		long s = this.duree % 60;

		if (m > 0) res += m + "min ";
		if (s > 0) res += s + "sec";
		
		return res;
	}
}