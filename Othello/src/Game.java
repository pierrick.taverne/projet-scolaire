import javax.swing.*;
import java.awt.*;

/**
 * Game est la classe representant la fenetre du jeu Othello.<br/>
 * Il utilise notamment les classes Othellier et MenuBar.
 * 
 * @see Othellier
 * @see MenuBar
 *
 * @author TAVERNE Pierrick
 * @version 1.0
 */

public class Game extends JFrame{
	private Othellier othellier;

	/**
	 * A la construction d'une game, il se construit une fenetre a
	 *	laquelle on ajoute un MenuBar et un Othellier.
	 *
	 * @param str
	 *			Nom de la fenetre.
	 *
	 */
	public Game(String str){
		super(str);
		setJMenuBar(new MenuBar());
		this.othellier = new Othellier(1);
		add(othellier);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Retourne l'othellier de la game.
	 *
	 * @return L'Othellier de la game.
	 */
	public Othellier getOthellier(){
		return this.othellier;
	}

	/**
	 * Permet d'instancier un nouvel Othellier lorqu'on debute une nouvelle partie.
	 *
	 * @param n
	 *			Version de l'Othellier souhaite.
	 *
	 * @see Othellier#Othellier
	 */
	public void changeOthellier(int n){
		getContentPane().removeAll();
		this.othellier = new Othellier(n);
		getContentPane().add(this.othellier);
		repaint();
		validate();
	}

	public static void main(String[] args){
		Game fenetre = new Game("Othello");
		fenetre.setSize(1500,990);
	}
}
