# -*- coding: utf-8 -*-
#TAVERNE Pierrick

#Constantes
EXT = "E"
MUR = "M"
SOL = "S"
CAISSE = "C"
OBJECTIF = "O"
JOUEUR = "J"

NIVEAU0 = (
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,MUR,MUR,MUR,MUR,MUR,EXT,EXT,EXT),
	(EXT,EXT,MUR,OBJECTIF,SOL,JOUEUR,MUR,EXT,EXT,EXT),
	(EXT,EXT,MUR,SOL,CAISSE,SOL,MUR,EXT,EXT,EXT),
	(EXT,EXT,MUR,SOL,SOL,SOL,MUR,EXT,EXT,EXT),
	(EXT,EXT,MUR,MUR,MUR,MUR,MUR,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
)
NIVEAU1 = (
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,MUR,MUR,MUR,MUR,MUR,MUR,MUR,EXT,EXT),
	(EXT,MUR,OBJECTIF,SOL,CAISSE,SOL,OBJECTIF,MUR,EXT,EXT),
	(EXT,MUR,MUR,SOL,MUR,CAISSE,SOL,MUR,EXT,EXT),
	(EXT,EXT,MUR,SOL,JOUEUR,SOL,SOL,MUR,EXT,EXT),
	(EXT,EXT,MUR,MUR,MUR,MUR,MUR,MUR,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
)
NIVEAU2 = (
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,MUR,MUR,MUR,MUR,MUR,EXT,EXT,EXT,EXT),
	(EXT,MUR,SOL,SOL,SOL,MUR,EXT,EXT,EXT,EXT),
	(EXT,MUR,OBJECTIF,SOL,CAISSE,MUR,MUR,MUR,EXT,EXT),
	(EXT,MUR,OBJECTIF,SOL,JOUEUR,SOL,SOL,MUR,EXT,EXT),
	(EXT,MUR,MUR,CAISSE,SOL,MUR,SOL,MUR,EXT,EXT),
	(EXT,EXT,MUR,SOL,SOL,SOL,SOL,MUR,EXT,EXT),
	(EXT,EXT,MUR,MUR,MUR,MUR,MUR,MUR,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
)
NIVEAU3 = (
	(EXT,EXT,EXT,MUR,MUR,MUR,MUR,MUR,EXT,EXT),
	(EXT,MUR,MUR,MUR,SOL,SOL,SOL,MUR,EXT,EXT),
	(EXT,MUR,OBJECTIF,JOUEUR,CAISSE,SOL,SOL,MUR,EXT,EXT),
	(EXT,MUR,MUR,MUR,SOL,CAISSE,OBJECTIF,MUR,EXT,EXT),
	(EXT,MUR,OBJECTIF,MUR,MUR,MUR,SOL,MUR,EXT,EXT),
	(EXT,MUR,SOL,MUR,SOL,OBJECTIF,SOL,MUR,MUR,EXT),
	(EXT,MUR,CAISSE,SOL,CAISSE,CAISSE,CAISSE,OBJECTIF,MUR,EXT),
	(EXT,MUR,SOL,SOL,SOL,OBJECTIF,SOL,SOL,MUR,EXT),
	(EXT,MUR,MUR,MUR,MUR,MUR,MUR,MUR,MUR,EXT),
	(EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT,EXT),
)

LISTE_NIVEAUX = [NIVEAU0,NIVEAU1,NIVEAU2,NIVEAU3]


#Classe du jeu
class Sokoban:

	#Constructeur
	def __init__(self, niveau):

		#Grille du niveau
		self.niveau = niveau
		self.grille = []
		for i in range(len(self.niveau)):
			uneLigne = []
			for j in range(len(self.niveau[i])):
				uneLigne.append(self.niveau[i][j])
			self.grille.append(uneLigne)

		#Liste des cases objectif
		self.objectifs = []
		for i in range(len(niveau)):
			for j in range(len(niveau[i])):
				if niveau[i][j] == OBJECTIF:
					self.objectifs.append((i,j))
				if niveau[i][j] == JOUEUR:
					self.joueur = [i,j]
					self.departJoueur = (i,j)

	def coordDirection(self, entry):
		"""Sokoban, str -> (int, int)

		Retourne les coordonnée de direction selon le le cracatère
		g = gauche; h = haut; d = droite; b = bas"""

		if entry.lower() == 'g':
			return (0,-1)
		elif entry.lower() == 'h':
			return (-1,0)
		elif entry.lower() == 'd':
			return (0,1)
		elif entry.lower() == 'b':
			return (1,0)
		else:
			return (0,0)

	def deplacement(self, coord):
		"""Sokoban, (int, int) -> None

		Effectue le déplacement si c'est possible"""

		j0 = self.joueur[0]
		j1 = self.joueur[1]
		x = self.joueur[0]+coord[0]
		y = self.joueur[1]+coord[1]
		dest = self.grille[x][y]
		if dest == OBJECTIF or dest == SOL:
			self.grille[x][y] = JOUEUR
			self.joueur = [x,y]
			if (j0,j1) in self.objectifs:
				self.grille[j0][j1] = OBJECTIF
			else:
				self.grille[j0][j1] = SOL
		elif dest == CAISSE:
			a = x+coord[0]
			b = y+coord[1]
			dest2 = self.grille[a][b]
			if dest2 == OBJECTIF or dest2 == SOL:
				self.grille[a][b] = CAISSE
				self.grille[x][y] = JOUEUR
				self.joueur = [x,y]
				if (j0,j1) in self.objectifs:
					self.grille[j0][j1] = OBJECTIF
				else:
					self.grille[j0][j1] = SOL

	def toStr(self):
		"""Sokoban -> None

		Affiche la grille"""

		for i in range(len(self.grille)):
			print(self.grille[i])

	def fini(self):
		"""Sokoban -> bool

		Retourne True si les caisses sont sur les objectifs, False sinon"""

		for couple in self.objectifs:
			if self.grille[couple[0]][couple[1]] != CAISSE:
				return False
		return True

	def reset(self):
		"""Sokoban -> None

		Reinitialise la position des caisses et du joueur"""

		self.grille = []
		for i in range(len(self.niveau)):
			uneLigne = []
			for j in range(len(self.niveau[i])):
				uneLigne.append(self.niveau[i][j])
			self.grille.append(uneLigne)
		self.joueur = [self.departJoueur[0],self.departJoueur[1]]










#Fonction du jeu pour jouer en console

def main():
	for i in range(len(LISTE_NIVEAUX)):
		print("Niveau: "+str(i))
		s = Sokoban(LISTE_NIVEAUX[i])
		s.toStr()
		while not s.fini():
			direction = input("Direction du joueur (r pour reset): ")
			if direction.lower() == "r":
				s.reset()
				s.toStr()
			else:
				s.deplacement(s.coordDirection(direction))
				s.toStr()