# -*- coding: utf-8 -*-
#TAVERNE Pierrick

#Import de bibliothèque
from tkinter import *
from sokoban import *

#Classe du jeu
class VueSokoban:

	#Constructeur
	def __init__(self):

		#Attributs
		self.jeu = Sokoban(LISTE_NIVEAUX[0])

		self.numNiveau = 0

		self.bool = True

		#Affichage
		self.fen = Tk()
		self.fen.title("Sokoban")

		#Frame pour le texte
		self.console = Frame(self.fen)
		self.console.pack()

		self.label_init = Label(self.console, text = "Le déplacement se fait avec les touches fléchées. \n L'objectif est de mettre les caisses (carré vert) sur les objectifs (carré rouge).")
		self.label_init.pack()

		self.label = Label(self.console, text = "NIVEAU 0")
		self.label.pack()

		self.nbCoups = 0
		self.labelCoups = Label(self.console, text = "Nombre de coups: "+str(self.nbCoups))
		self.labelCoups.pack()

		#Frame pour la grille
		self.grille = Canvas(self.fen, width = len(self.jeu.grille)*50, height = len(self.jeu.grille[0])*50)
		self.cases = []
		x = 0
		y = 0
		for i in range(len(self.jeu.grille)):
			uneLigne = []
			for j in range(len(self.jeu.grille[i])):
				if self.jeu.grille[i][j] == MUR:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "black")
				elif self.jeu.grille[i][j] == EXT:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "grey")
				elif self.jeu.grille[i][j] == SOL:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "white")
				elif self.jeu.grille[i][j] == CAISSE:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "green")
				elif self.jeu.grille[i][j] == OBJECTIF:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "red")
				elif self.jeu.grille[i][j] == JOUEUR:
					case = self.grille.create_rectangle(j*50,i*50,(j+1)*50,(i+1)*50, fill = "blue")
				uneLigne.append(case)
				y += 1
			self.cases.append(uneLigne)
			x += 1
			y = 0

		self.grille.pack()

		#Action des boutons de déplacement
		self.fen.bind("<Key-Left>",self.etapeGauche)
		self.fen.bind("<Key-Up>",self.etapeHaut)
		self.fen.bind("<Key-Right>",self.etapeDroite)
		self.fen.bind("<Key-Down>",self.etapeBas)

		#Bouton pour reset
		self.fen.bind("<Key-r>",self.reset)

		#Bouton pour passer au niveau suivant
		self.fen.bind("<Key-Return>",self.niveauSuivant)

		#Boucle principale
		self.fen.mainloop()

	def afficheGrille(self):
		"""VueSokoban -> None

		Modifie les canvas"""

		x = 0
		y = 0
		for i in range(len(self.jeu.grille)):
			for j in range(len(self.jeu.grille[i])):
				if self.jeu.grille[i][j] == MUR:
					self.grille.itemconfigure(self.cases[i][j], fill = "black")
				elif self.jeu.grille[i][j] == EXT:
					self.grille.itemconfigure(self.cases[i][j], fill = "grey")
				elif self.jeu.grille[i][j] == SOL:
					self.grille.itemconfigure(self.cases[i][j], fill = "white")
				elif self.jeu.grille[i][j] == CAISSE:
					self.grille.itemconfigure(self.cases[i][j], fill = "green")
				elif self.jeu.grille[i][j] == OBJECTIF:
					self.grille.itemconfigure(self.cases[i][j], fill = "red")
				elif self.jeu.grille[i][j] == JOUEUR:
					self.grille.itemconfigure(self.cases[i][j], fill = "blue")
				y += 1
			x += 1
			y = 0

	def etapeGauche(self, event):
		if self.bool:
			self.jeu.deplacement(self.jeu.coordDirection('g'))
			self.etape()

	def etapeHaut(self, event):
		if self.bool:
			self.jeu.deplacement(self.jeu.coordDirection('h'))
			self.etape()


	def etapeDroite(self, event):
		if self.bool:
			self.jeu.deplacement(self.jeu.coordDirection('d'))
			self.etape()

	def etapeBas(self, event):
		if self.bool:
			self.jeu.deplacement(self.jeu.coordDirection('b'))
			self.etape()

	def etape(self):
		"""VueSokoban -> None

		Action qui a lieu à chaque déplacement"""

		self.afficheGrille()
		self.fini()
		self.nbCoups += 1
		self.labelCoups["text"] = "Nombre de coups: "+str(self.nbCoups)

	def reset(self, event):
		self.jeu.reset()
		self.afficheGrille()
		self.bool = True
		self.label["text"] = "NIVEAU "+str(self.numNiveau)
		self.nbCoups = 0
		self.labelCoups["text"] = "Nombre de coups: "+str(self.nbCoups)

	def fini(self):
		"""VueSokoban -> None

		Test si la partie est fini"""

		if self.jeu.fini():
			if self.numNiveau == len(LISTE_NIVEAUX)-1:
				self.label["text"] += ". Terminé !\nVous avez terminé le jeu !"
			else:
				self.label["text"] += ". Terminé !\nAppuyer sur entrer pour passer au niveau suivant"
			self.bool = False

	def niveauSuivant(self, event):
		if self.jeu.fini() and self.numNiveau < len(LISTE_NIVEAUX)-1:
			self.numNiveau += 1
			self.jeu = Sokoban(LISTE_NIVEAUX[self.numNiveau])
			self.afficheGrille()
			self.label["text"] = "NIVEAU "+str(self.numNiveau)
			self.bool = True
			self.nbCoups = 0
			self.labelCoups["text"] = "Nombre de coups: "+str(self.nbCoups)