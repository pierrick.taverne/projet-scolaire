//TAVERNE Pierrick

var bool = false;

function Tile (id) {
	this.id = id;
	this.x;
	this.y;
	this.isRevealed = false;
}

Tile.prototype = {
	setCoord: function (x,y) {
		this.x = x;
		this.y = y;
	},

	display: function () {
		var id_img = "img"+this.x+this.y;
		var img = document.getElementById(id_img);
		if (this.isRevealed == true) {
			img.src = "images/"+this.id+".jpg";
		}else{
			img.src = "images/couleur.jpg";
		}
	},

	flipTile: function () {
		if (this.isRevealed == true) {
			this.isRevealed = false;
		}else{
			this.isRevealed = true;
		}
	},
}

function Board () {
	this.tiles = [];
}

Board.prototype = {
	display: function () {
		var img = "images/couleur.jpg";
		var board = document.getElementById("board");
		board.innerHTML = "";
		this.tiles.forEach(function (element, index) {
			var i = document.createElement("img");
			i.id = "img"+element.x+element.y;
			i.src = img;
			//i.src = "images/"+element.id+".jpg";
			i.style = "position: absolute; height: 100px; width: 100px; left: "+element.x+"px; top: "+element.y+"px;";
			board.appendChild(i);
			if (index % 4 == 0) {
				board.appendChild(document.createElement("br"));
			}
			i.addEventListener("click", function () {flip(element, bool)}, false);
		})
	},

	reset: function () {
		this.tiles = [];

		var x = 1;
		for (var i = 1; i <= 26; i++) {
			this.tiles.push(new Tile ("img"+x));
			if (i % 2 == 0) {
				x++;
			}
		}

		x = Math.floor(Math.random() * 26);
		this.tiles.splice(x, 1);

		for (i = 0; i < 10; i++) {
			var j = 0;
			var valL = '';
			var valJ = valL;
			var l = this.tiles.length - 1;
			while (l > -1) {
				j = Math.floor(Math.random() * l);
				valL = this.tiles[l];
				valJ = this.tiles[j];
				this.tiles[l] = valJ;
				this.tiles[j] = valL;
				l = l - 1;
			}
		}

		this.tiles.forEach(function (element, index) {
			var coordX = (index % 5) * 110;
			if (index < 5) {
				var coordY = 0;
			}else if (index < 10) {
				var coordY = 110;
			}else if (index < 15) {
				var coordY = 220;
			}else if (index < 20) {
				var coordY = 330;
			}else{
				var coordY = 440;
			}
			element.setCoord(coordX, coordY);
		})
	},
}

function flip (element, bool) {
	if (element.isRevealed == false && bool == true) {
		element.flipTile();
		element.display();
		}
}





























function ajoutePaire (paire, i, element) {
	paire.push(i);
	paire.push(element);
	if (paire.length == 4) {
		bool = false;
	}
}











function main () {
	var div_board = document.getElementById("board");
	var board = new Board();
	board.reset();
	board.display();

	var btnGo = document.getElementById("go");
	var formModeJeu = document.getElementById("ModeJeu");
	var div_jeu = document.getElementById("jeu");

	btnGo.addEventListener("click", function () {
		document.getElementById("img00").click();
		board.reset();
		board.display();

		var choix = formModeJeu.modeJeu.value;
		var fini = false;
		var paire = [];
		bool = true;
		div_jeu.innerHTML = "";

		formModeJeu.modeJeu.forEach(function (element) {
			element.disabled = true;
		})
		btnGo.disabled = true;

		board.tiles.forEach(function (element) {
			var i = document.getElementById("img"+element.x+element.y);
			i.addEventListener("click", function () {ajoutePaire(paire, i, element)}, false)
		});

		switch (choix) {
			case "1":
				function contreLaMontre () {
					var label_dcpt = document.createElement("label");
					label_dcpt.id = "label_dcpt";
					label_dcpt.innerHTML = "";
					div_jeu.appendChild(label_dcpt);
					var time_dcpt = 120;
					var temps = 120;
					var x;
					var score = 0;
					var nb_coup = 0;

					function decompte () {
						if (paire.length == 4 && paire[0].src == paire[2].src) {
							var img1 = paire[0].cloneNode();
							var img2 = paire[2].cloneNode();
							div_board.insertBefore(img1, paire[0]);
							div_board.insertBefore(img2, paire[2]);
							paire[0].parentNode.removeChild(paire[0]);
							paire[2].parentNode.removeChild(paire[2]);
							paire = [];
							score++;
							nb_coup++;
							bool = true;
						}else if (paire.length == 4) {
							var time = window.setTimeout(function () {
								paire[1].flipTile();
								paire[1].display();
								paire[3].flipTile();
								paire[3].display();
								paire = [];
								bool = true;
								nb_coup++;
							}, 1000);
						}

						if (score == 12) {
							fini = true;
						}

						var label_dcpt = document.getElementById("label_dcpt");
						if (time_dcpt > 1 && !fini) {
							label_dcpt.innerHTML = Math.round(time_dcpt) + " secondes.<br>";
							time_dcpt -= 1;
							x = setTimeout(decompte,1000);
						}else if (time_dcpt == 1 && !fini) {
							label_dcpt.innerHTML = Math.round(time_dcpt) + " seconde.<br>";
							time_dcpt -= 1;
							x = setTimeout(decompte,1000);
						}else{
							label_dcpt.innerHTML = "Terminé.<br>";
							formModeJeu.modeJeu.forEach(function (element) {
								element.disabled = false;
							})
							btnGo.disabled = false;
							bool = false;
							var time_final = temps-Math.round(time_dcpt);
							alert("Fini en "+time_final+" secondes. Nombre de coups: "+nb_coup);
						}
					}
					decompte();
				}
				contreLaMontre();
				break;
			case "2":
				function deuxJoueurs () {
					var label_joueur1 = document.createElement("label");
					var label_joueur2 = document.createElement("label");
					label_joueur1.id = "joueur1";
					label_joueur1.style.backgroundColor = "#ECA706";
					label_joueur2.id = "joueur2";
					label_joueur1.innerHTML = "Joueur 1<br>Score: 0<br><br>";
					label_joueur2.innerHTML = "Joueur 2<br>Score: 0";
					div_jeu.appendChild(label_joueur1);
					div_jeu.appendChild(label_joueur2);
					var score_joueur1 = 0;
					var score_joueur2 = 0;
					var joueur = 1;
					var x;

					function joue () {
						if (paire.length == 4 && paire[0].src == paire[2].src) {
							var img1 = paire[0].cloneNode();
							var img2 = paire[2].cloneNode();
							div_board.insertBefore(img1, paire[0]);
							div_board.insertBefore(img2, paire[2]);
							paire[0].parentNode.removeChild(paire[0]);
							paire[2].parentNode.removeChild(paire[2]);
							paire = [];
							bool = true;
							if (joueur == 1) {
								score_joueur1++;
								label_joueur1.innerHTML = "Joueur 1<br>Score: "+score_joueur1+"<br><br>";
								img1.style.borderColor = "#ECA706";
								img2.style.borderColor = "#ECA706";
							}else{
								score_joueur2++;
								label_joueur2.innerHTML = "Joueur 2<br>Score: "+score_joueur2;
								img1.style.borderColor = "#DE0036";
								img2.style.borderColor = "#DE0036";
							}
						}else if (paire.length == 4) {
							var time = window.setTimeout(function () {
								paire[1].flipTile();
								paire[1].display();
								paire[3].flipTile();
								paire[3].display();
								paire = [];
								bool = true;
								if (joueur == 1) {
									joueur = 2;
									label_joueur2.style.backgroundColor = "#DE0036";
									label_joueur1.style.backgroundColor = "#000000";
								}else{
									joueur = 1;
									label_joueur1.style.backgroundColor = "#ECA706";
									label_joueur2.style.backgroundColor = "#000000";
								}
							}, 1000);
						}

						if (score_joueur1 + score_joueur2 != 12) {
							x = setTimeout(joue, 1000);
						}else{
							formModeJeu.modeJeu.forEach(function (element) {
								element.disabled = false;
							})
							btnGo.disabled = false;
							bool = false;
							if (score_joueur1 > score_joueur2) {
								alert("Le gagnant est le joueur 1 !");
							}else if (score_joueur1 < score_joueur2) {
								alert("Le gagnant est le joueur 2 !");
							}else{
								alert("Egalité.");
							}
						}
					}
					joue();
				}
				deuxJoueurs();
				break;
			case "3":
				function contreIA () {
					var label_joueur = document.createElement("label");
					var label_ordinateur = document.createElement("label");
					label_joueur.id = "joueur";
					label_joueur.style.backgroundColor = "#ECA706";
					label_ordinateur.id = "ordinateur";
					label_joueur.innerHTML = "Joueur<br>Score: 0<br><br>";
					label_ordinateur.innerHTML = "Ordinateur<br>Score: 0";
					div_jeu.appendChild(label_joueur);
					div_jeu.appendChild(label_ordinateur);
					var score_joueur = 0;
					var score_ordinateur = 0;
					var joueur = 1;
					var x;
					var alea = 0;

					function joue () {
						if (joueur == 2) {
							while (alea < 2) {
								var tile_alea = Math.floor(Math.random() * board.tiles.length);
								if (board.tiles[tile_alea].isRevealed == false) {
									flip(board.tiles[tile_alea], bool);
									ajoutePaire(paire, document.getElementById("img"+board.tiles[tile_alea].x+board.tiles[tile_alea].y), board.tiles[tile_alea]);
									alea++;
								}
							}
							alea = 0;
						}
						if (paire.length == 4 && paire[0].src == paire[2].src) {
							var img1 = paire[0].cloneNode();
							var img2 = paire[2].cloneNode();
							div_board.insertBefore(img1, paire[0]);
							div_board.insertBefore(img2, paire[2]);
							paire[0].parentNode.removeChild(paire[0]);
							paire[2].parentNode.removeChild(paire[2]);
							paire = [];
							bool = true;
							if (joueur == 1) {
								score_joueur++;
								label_joueur.innerHTML = "Joueur<br>Score: "+score_joueur+"<br><br>";
								img1.style.borderColor = "#ECA706";
								img2.style.borderColor = "#ECA706";
							}else{
								score_ordinateur++;
								label_ordinateur.innerHTML = "Ordinateur<br>Score: "+score_ordinateur;
								img1.style.borderColor = "#DE0036";
								img2.style.borderColor = "#DE0036";
							}
						}else if (paire.length == 4) {
							var time = window.setTimeout(function () {
								paire[1].flipTile();
								paire[1].display();
								paire[3].flipTile();
								paire[3].display();
								paire = [];
								bool = true;
								if (joueur == 1) {
									joueur = 2;
									label_ordinateur.style.backgroundColor = "#DE0036";
									label_joueur.style.backgroundColor = "#000000";
								}else{
									joueur = 1;
									label_joueur.style.backgroundColor = "#ECA706";
									label_ordinateur.style.backgroundColor = "#000000";
								}
							}, 1000);
						}

						if (score_joueur + score_ordinateur != 12) {
							x = setTimeout(joue, 1000);
						}else{
							formModeJeu.modeJeu.forEach(function (element) {
								element.disabled = false;
							})
							btnGo.disabled = false;
							bool = false;
							if (score_joueur > score_ordinateur) {
								alert("Le gagnant est le joueur !");
							}else if (score_joueur < score_ordinateur) {
								alert("Le gagnant est l'ordinateur !");
							}else{
								alert("Egalité.");
							}
						}
					}
					joue();
				}
				contreIA();
				break;
			default:
				break;
		}
	}, false);
}



window.onload = main;